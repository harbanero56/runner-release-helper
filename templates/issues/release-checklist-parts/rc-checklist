<!-- this is rc{{.RC}} part -->

{{ $branch := tenary (eq .RC 1) "main" (printf "%d-%d-stable" .Major .Minor) }}
{{ $helmChartBranch := tenary (eq .RC 1) "main" (printf "%d-%d-0-stable" .HelmChartMajor .HelmChartMinor) }}

{{ if eq .RC 1 }}
## First working day after 7th - **v{{.Major}}.{{.Minor}}.0-rc1 release**
{{ else }}
## **v{{.Major}}.{{.Minor}}.0-rc{{.RC}}** release
{{ end }}

- [ ] check if Pipeline for `{{$branch}}` is passing: [![pipeline status](https://gitlab.com/gitlab-org/gitlab-runner/badges/{{$branch}}/pipeline.svg)](https://gitlab.com/gitlab-org/gitlab-runner/commits/{{$branch}})
    - [ ] add all required fixes to make `{{$branch}}` Pipeline passing
- [ ] `git checkout {{$branch}} && git pull` in your local working copy!
- [ ] prepare CHANGELOG entries

    ```bash
    make generate_changelog CHANGELOG_RELEASE=v{{.Major}}.{{.Minor}}.0-rc{{.RC}}
    ```

- [ ] add **v{{.Major}}.{{.Minor}}.0-rc{{.RC}}** CHANGELOG entries and commit

    ```bash
    git add CHANGELOG.md && \
    git commit -m "Update CHANGELOG for v{{.Major}}.{{.Minor}}.0-rc{{.RC}}" -S
    ```

- [ ] tag and push **v{{.Major}}.{{.Minor}}.0-rc{{.RC}}**:

    ```bash
    git tag -s v{{.Major}}.{{.Minor}}.0-rc{{.RC}} -m "Version v{{.Major}}.{{.Minor}}.0-rc{{.RC}}" && \
    git push origin v{{.Major}}.{{.Minor}}.0-rc{{.RC}}
    ```

{{ if eq .RC 1 }}
- [ ] create and push `{{.Major}}-{{.Minor}}-stable` branch:

    ```bash
    git checkout -b {{.Major}}-{{.Minor}}-stable && \
    git push -u origin {{.Major}}-{{.Minor}}-stable
    ```

- [ ] checkout to `main`, update `VERSION` file to `{{.Major}}.{{inc .Minor}}.0` and push `main`:

    ```bash
    git checkout main; echo -n "{{.Major}}.{{inc .Minor}}.0" > VERSION && \
    git add VERSION && \
    git commit -m "Bump version to {{.Major}}.{{inc .Minor}}.0" -S && \
    git push
    ```
{{ else }}
- [ ] push `{{.Major}}-{{.Minor}}-stable` branch:

    ```bash
    git push origin {{.Major}}-{{.Minor}}-stable
    ```
{{ end }}

- [ ] wait for Pipeline for `v{{.Major}}.{{.Minor}}.0-rc{{.RC}}` to pass [![pipeline status](https://gitlab.com/gitlab-org/gitlab-runner/badges/v{{.Major}}.{{.Minor}}.0-rc{{.RC}}/pipeline.svg)](https://gitlab.com/gitlab-org/gitlab-runner/commits/v{{.Major}}.{{.Minor}}.0-rc{{.RC}})
    - [ ] add all required fixes to make `v{{.Major}}.{{.Minor}}.0-rc{{.RC}}` passing
- [ ] update runner [helm chart](https://gitlab.com/gitlab-org/charts/gitlab-runner) to use `v{{.Major}}.{{.Minor}}.0-rc{{.RC}}` version
    - [ ] check if Pipeline for `{{$helmChartBranch}}` is passing: [![pipeline status](https://gitlab.com/gitlab-org/charts/gitlab-runner/badges/{{$helmChartBranch}}/pipeline.svg)](https://gitlab.com/gitlab-org/charts/gitlab-runner/commits/{{$helmChartBranch}})
        - [ ] add all required fixes to make `{{$helmChartBranch}}` Pipeline passing
    - [ ] go to your local working copy of https://gitlab.com/gitlab-org/charts/gitlab-runner
    - [ ] `git checkout {{$helmChartBranch}} && git pull` in your local working copy!
    - [ ] set Helm Chart to use `v{{.Major}}.{{.Minor}}.0-rc{{.RC}}` version of GitLab Runner

        ```bash
        sed -i".bak" "s/^appVersion: .*/appVersion: {{.Major}}.{{.Minor}}.0-rc{{.RC}}/" Chart.yaml && \
        rm Chart.yaml.bak && \
        git add Chart.yaml && \
        git commit -m "Update used GitLab Runner version to {{.Major}}.{{.Minor}}.0-rc{{.RC}}" -S
        ```

    - [ ] bump version of the Helm Chart to `{{.HelmChartMajor}}.{{.HelmChartMinor}}.0-rc{{.RC}}`

        ```bash
        sed -i".bak" "s/^version: .*/version: {{.HelmChartMajor}}.{{.HelmChartMinor}}.0-rc{{.RC}}/" Chart.yaml && \
        rm Chart.yaml.bak && \
        git add Chart.yaml && \
        git commit -m "Bump version to {{.HelmChartMajor}}.{{.HelmChartMinor}}.0-rc{{.RC}}" -S
        ```

    - [ ] prepare CHANGELOG entries

        ```bash
        make generate_changelog CHANGELOG_RELEASE=v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0-rc{{.RC}}
        ```

        - [ ] manually add line including GitLab Runner version update information as the first item under `New features`

            ```markdown
            - Update GitLab Runner version to {{.Major}}.{{.Minor}}.0-rc{{.RC}}
            ```

    - [ ] add **v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0-rc{{.RC}}** CHANGELOG entries and commit

        ```bash
        git add CHANGELOG.md && \
        git commit -m "Update CHANGELOG for v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0-rc{{.RC}}" -S
        ```

    - [ ] tag and push **v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0-rc{{.RC}}**:

        ```bash
        git tag -s v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0-rc{{.RC}} -m "Version v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0-rc{{.RC}}" && \
        git push origin v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0-rc{{.RC}}
        ```

{{ if eq .RC 1 }}
    - [ ] create and push `{{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable` branch:

        ```bash
        git checkout -b {{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable && \
        git push -u origin {{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable
        ```

    - [ ] checkout to `main`, bump version of the Helm Chart to `{{.HelmChartMajor}}.{{inc .HelmChartMinor}}.0-beta`, set back
          the `bleeding` version of Runner and push `main`:

        ```bash
        git checkout main && \
        sed -i".bak" "s/^version: .*/version: {{.HelmChartMajor}}.{{inc .HelmChartMinor}}.0-beta/" Chart.yaml && \
        rm Chart.yaml.bak && \
        sed -i".bak" "s/^appVersion: .*/appVersion: bleeding/" Chart.yaml && \
        rm Chart.yaml.bak && \
        git add Chart.yaml && \
        git commit -m "Bump version to {{.HelmChartMajor}}.{{inc .HelmChartMinor}}.0-beta" -S && \
        git push
        ```

- [ ] inform the `g_runner` channel that the feature freeze is done

    > :snowflake: GitLab Runner `v{{.Major}}.{{.Minor}}.0-rc{{.RC}}` was tagged so the feature freeze is done! :snowflake:
    >
    > Everything merged to `main` as of this moment will be released with `v{{.Major}}.{{inc .Minor}}.0`.

- [ ] unassign yourself from this issue and assign to the release manager responsible for the deployment

{{ else }}
    - [ ] push **{{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable**:

        ```bash
        git push origin {{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable
        ```
{{ end }}

- [ ] deploy **v{{.Major}}.{{.Minor}}.0-rc{{.RC}}**

    Detailed description of Runners deployment for GitLab.com CI fleet can be found
    [in the runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/ci-runners/linux/deployment.md).

    - [ ] at RC{{.RC}} release day: to `prmX` runners

        - [ ] make sure it's not inside of the [PCL time window](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/ci-runners/README.md#production-change-lock-pcl).
        - [ ] go to your local [`chef-repo`](https://gitlab.com/gitlab-com/gl-infra/chef-repo) working directory and execute:

            ```bash
            knife ssh -afqdn 'roles:gitlab-runner-prm' -- 'sudo -i /root/runner_upgrade.sh stop_chef'
            knife ssh -afqdn 'roles:gitlab-runner-prm' -- 'sudo -i systemctl is-active chef-client'
            git checkout master && git pull
            git checkout -b update-prm-runners-to-{{.Major}}-{{.Minor}}-0-rc{{.RC}}
            ```

        - [ ] update version

            ```bash
            $EDITOR roles/gitlab-runner-prm.json
            ```

            In the role definition prepare the `override_attributes` entry. It should be placed
            at the top of the file:

            ```json
            "override_attributes": {
              "cookbook-gitlab-runner": {
                "gitlab-runner": {
                    "repository": "unstable",
                    "version": "{{.Major}}.{{.Minor}}.0-rc{{.RC}}"
                }
              }
            },
            ```

        - [ ] `git add roles/gitlab-runner-prm.json && git commit -m "Update prmX runners to v{{.Major}}.{{.Minor}}.0-rc{{.RC}}"`
        - [ ] `git push -u origin update-prm-runners-to-{{.Major}}-{{.Minor}}-0-rc{{.RC}} -o merge_request.create -o merge_request.label="deploy" -o merge_request.label="group::runner"`
        - [ ] inform the EOC `@sre-oncall` inside of [`#production`](https://gitlab.slack.com/archives/C101F3796) slack channel: `@sre-oncall I'll be starting the deploy for v{{.Major}}.{{.Minor}}.0-rc{{.RC}} on prmX, as part of the monthly release. $LINK_TO_CHEF_REPO_MERGE_REQUEST`
        - [ ] merge the [`chef-repo` MR](https://gitlab.com/gitlab-com/gl-infra/chef-repo/merge_requests)
        - [ ] check the `production_dry_run` job if it tries to update only the changed role
        - [ ] start the manual `apply to prod` job
        - [ ] after the job is finished execute:

            ```bash
            knife ssh -C 1 -afqdn 'roles:gitlab-runner-prm' -- 'sudo -i /root/runner_upgrade.sh' &
            time wait
            ```

    - [ ] next day (if no problems): to rest of the runners

        - [ ] make sure it's not inside of the [PCL time window](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/ci-runners/README.md#production-change-lock-pcl).
        - [ ] go to your local [`chef-repo`](https://gitlab.com/gitlab-com/gl-infra/chef-repo) working directory and execute:

            ```bash
            knife ssh -afqdn 'roles:gitlab-runner-gsrm OR roles:gitlab-runner-srm OR roles:org-ci-base-runner' -- 'sudo -i /root/runner_upgrade.sh stop_chef'
            knife ssh -afqdn 'roles:gitlab-runner-gsrm OR roles:gitlab-runner-srm OR roles:org-ci-base-runner' -- 'sudo -i systemctl is-active chef-client'
            git checkout master && git pull
            git checkout -b update-runners-to-{{.Major}}-{{.Minor}}-0-rc{{.RC}}
            ```

        - [ ] update version for gsrm/srm

            ```bash
            $EDITOR roles/gitlab-runner-base.json
            ```

            In the role definition prepare the `gitlab-runner` entry:

            ```json
            "cookbook-gitlab-runner": {
              "gitlab-runner": {
                "repository": "unstable",
                "version": "{{.Major}}.{{.Minor}}.0-rc{{.RC}}"
              }
            }
            ```

        - [ ] update version for org-ci

            ```bash
            $EDITOR roles/org-ci-base-runner.json
            ```

            In the role definition prepare the `gitlab-runner` entry:

            ```json
            "cookbook-gitlab-runner": {
              "gitlab-runner": {
                "repository": "unstable",
                "version": "{{.Major}}.{{.Minor}}.0-rc{{.RC}}"
              }
            }
            ```

        - [ ] remove overrides from `prmX` runners

            ```bash
            $EDITOR roles/gitlab-runner-prm.json
            ```

        - [ ] `git add roles/gitlab-runner-prm.json roles/gitlab-runner-base.json roles/org-ci-base-runner.json && git commit -m "Update runners to v{{.Major}}.{{.Minor}}.0-rc{{.RC}}"`
        - [ ] `git push -u origin update-runners-to-{{.Major}}-{{.Minor}}-0-rc{{.RC}} -o merge_request.create -o merge_request.label="deploy" -o merge_request.label="group::runner"`
        - [ ] inform the EOC `@sre-oncall` inside of [`#production`](https://gitlab.slack.com/archives/C101F3796) slack channel: `@sre-oncall I'll be starting the deploy for v{{.Major}}.{{.Minor}}.0-rc{{.RC}} on runner fleet, as part of the monthly release. $LINK_TO_CHEF_REPO_MERGE_REQUEST`
        - [ ] merge the [`chef-repo` MR](https://gitlab.com/gitlab-com/gl-infra/chef-repo/merge_requests)
        - [ ] check the `production_dry_run` job if it tries to update only the changed role
        - [ ] start the manual `apply to prod` job
        - [ ] after the job is finished execute (we're not touching `prmX` - they are already updated):

            ```bash
            knife ssh -C1 -afqdn 'roles:gitlab-runner-gsrm' -- 'sudo -i /root/runner_upgrade.sh' &
            knife ssh -C1 -afqdn 'roles:gitlab-runner-srm' -- 'sudo -i /root/runner_upgrade.sh' &
            knife ssh -C1 -afqdn 'roles:org-ci-base-runner' -- 'sudo  -i /root/runner_upgrade.sh' &
            time wait
            ```
    
    - [ ] unassign yourself from this issue and assign it to the release manager responsible for tagging the release

{{ if eq .RC 1 }}
_New features_ window is closed - things not merged into `main` up to
this day, will be released with next release.

{{ end }}

## Pick into stable branch for RC{{inc .RC}} phase

At this moment, until the next RC creation, we're in the _pick into stable phase_. The release manager should periodically
review the list of merged MRs with a _pick into X.Y_ label:

- [GitLab Runner ~"Pick into {{.Major}}.{{.Minor}}" Merge Requests](https://gitlab.com/gitlab-org/gitlab-runner/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name%5B%5D=Pick%20into%20{{.Major}}.{{.Minor}})
- [GitLab Runner Helm Chart ~"Pick into {{.HelmChartMajor}}.{{.HelmChartMinor}}" Merge Requests](https://gitlab.com/gitlab-org/charts/gitlab-runner/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Pick%20into%20{{.HelmChartMajor}}.{{.HelmChartMinor}})

**GitLab Runner MRs:**

{{ template "rc-mrs-list" .RunnerMRs }}

**GitLab Runner's Helm Chart MRs:**

{{ template "rc-mrs-list" .HelmChartMRs }}

<details>
<summary>For each such Merge Request the release manager should (reveal for details)</summary>

1. Find the merge commit for the Merge Request
1. Copy the commit reference
1. Checkout to the branch:
    - `git checkout {{.Major}}-{{.Minor}}-stable && git pull` for GitLab Runner, or
    - `git checkout {{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable && git pull` for GitLab Runner's Helm Chart
1. Pick the merge commit with:

    ```bash
    git cherry-pick -m 1 [merge commit SHA here]
    ```

1. Remove the `Pick into X.Y` label from the Merge Request and leave a note that it was picked:

- for GitLab Runner use this message:

    ```markdown
    The MR was picked into `{{.Major}}-{{.Minor}}-stable` branch and will be released with `v{{.Major}}.{{.Minor}}.0-rc{{inc .RC}}`

    /unlabel ~"Pick into {{.Major}}.{{.Minor}}"
    ```

- for GitLab Runner's Helm Chart use this message:

    ```markdown
    The MR was picked into `{{.HelmChartMajor}}-{{.HelmChartMinor}}-0-stable` branch and will be released with `v{{.HelmChartMajor}}.{{.HelmChartMinor}}.0-rc{{inc .RC}}`

    /unlabel ~"Pick into {{.HelmChartMajor}}.{{.HelmChartMinor}}"
    ```

</details>

<details>
<summary>To prepare the `Pick into ...` MRs list execute:</summary>

```bash
rrhelper \
    list-pick-mrs-for-rc \
    --non-interactive \
    --do-not-create \
    --major {{.Major}} \
    --minor {{.Minor}}  \
    --helm-chart-major {{.HelmChartMajor}} \
    --helm-chart-minor {{.HelmChartMinor}} \
    {{.ReleaseChecklistIssueID}} {{inc .RC}}
```

</details>

<!-- next_rc -->
## RC{{inc .RC}} (if needed)

If any commit was picked into the stable branch since RC{{.RC}} was released, RC{{inc .RC}} version **must be released**
no later than at 18th day of the month or immediately if changes were picked after 18th.

If you need to create RC{{inc .RC}} version, then please execute this and follow the created checklist for RC{{inc .RC}}:

```bash
rrhelper \
    create-next-rc-checklist \
    --non-interactive \
    --do-not-create \
    --major {{.Major}} \
    --minor {{.Minor}}  \
    --helm-chart-major {{.HelmChartMajor}} \
    --helm-chart-minor {{.HelmChartMinor}} \
    {{.ReleaseChecklistIssueID}} {{inc .RC}}
```

<!-- next_rc_end -->
