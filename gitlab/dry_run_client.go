package gitlab

import (
	"fmt"
)

type DryRunClient struct{}

func (c *DryRunClient) CreateBranch(params CreateBranchParams) (Branch, error) {
	template := `
DRY-RUN Branch Creation
-----------------------

projectID: %s
branchName: %s

`
	fmt.Printf(template, params.ProjectID, params.Name)

	branch := Branch{
		Name:    "fake-name",
		Project: "fake-project",
	}

	return branch, nil
}

func (c *DryRunClient) CreateMergeRequest(params CreateMergeRequestParams) (MergeRequest, error) {
	template := `
DRY-RUN Merge Request Creation
------------------------------

projectID: %s
title: %s
sourceBranch: %s
targetBranch: %s
remove: %v
description: >>>

%s

<<<

`
	fmt.Printf(template, params.ProjectID, params.Title, params.SourceBranch, params.TargetBranch, params.Remove, params.Description)

	mergeRequest := MergeRequest{
		ID:          1234,
		IID:         1,
		Title:       "fake-merge-request",
		Description: params.Description,
		WebURL:      "http://gitlab.example.com/fake-ns/fake-project/merge_requests/1",
	}

	return mergeRequest, nil
}

func (c *DryRunClient) ListMergeRequests(params ListMergeRequestsParams) (MergeRequests, error) {
	template := `
DRY-RUN Merge Request Listing
-----------------------------

projectID: %s
milestone: %s
label: %s
state: %s

`
	fmt.Printf(template, params.ProjectID, params.Milestone, params.Label, params.State)

	mergeRequests := MergeRequests{
		{
			ID:          1234,
			IID:         1,
			Title:       "fake-merge-request",
			Description: "fake description",
			WebURL:      "http://gitlab.example.com/fake-ns/fake-project/merge_requests/1",
		},
	}

	return mergeRequests, nil
}

func (c *DryRunClient) CreateIssue(params CreateIssueParams) (Issue, error) {
	template := `
DRY-RUN ISSUE CREATION
----------------------

projectID: %s
title: %s
description: >>>

%s

<<<

`
	fmt.Printf(template, params.ProjectID, params.Title, params.Description)

	issue := Issue{
		ID:          1234,
		IID:         1,
		Title:       "fake-issue",
		Description: "fake-description",
		WebURL:      "http://gitlab.example.com/fake-ns/fake-project/issues/1",
	}

	return issue, nil
}

func (c *DryRunClient) UpdateIssue(params UpdateIssueParams) (Issue, error) {
	template := `
DRY-RUN ISSUE UPDATE
--------------------

projectID: %s
IID: %d
title: %s
description: >>>

%s

<<<

`
	fmt.Printf(template, params.ProjectID, params.IID, params.Title, params.Description)

	issue := Issue{
		ID:          1234,
		IID:         1,
		Title:       "fake-issue",
		Description: "fake-description",
		WebURL:      "http://gitlab.example.com/fake-ns/fake-project/issues/1",
	}

	return issue, nil
}

func (c *DryRunClient) ReadIssue(params ReadIssueParams) (Issue, error) {
	template := `
DRY-RUN Issue Reading
---------------------

projectID: %s
IID: %d

`
	fmt.Printf(template, params.ProjectID, params.IID)

	issue := Issue{
		ID:    1234,
		IID:   1,
		Title: "fake-issue",
		Description: `fake-description with link_example_placeholder link

<!-- next_rc -->
next RC

description
<!-- next_rc_end -->

something else
`,
		WebURL: "http://gitlab.example.com/fake-ns/fake-project/issues/1",
	}

	return issue, nil
}

func (c *DryRunClient) ReadRemoteFile(params ReadRemoteFileParams) ([]byte, error) {
	template := `
DRY-RUN Remote Raw File Reading
-------------------------------

projectID: %s
path: %s

`
	fmt.Printf(template, params.ProjectID, params.Path)

	return []byte{}, nil
}

func (c *DryRunClient) CreateLabel(params CreateLabelParams) (Label, error) {
	template := `
DRY-RUN Label creation
----------------------

projectID: %s
Name: %s
Description: %s
Color: %s

`
	fmt.Printf(template, params.ProjectID, params.Name, params.Description, params.Color)

	return Label{Name: params.Name}, nil
}
