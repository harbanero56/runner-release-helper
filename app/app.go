package app

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"

	rrhelper "gitlab.com/gitlab-org/ci-cd/runner-release-helper"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/gitlab"
)

const (
	gitLabBaseURL = "https://gitlab.com/"

	dryRunOption                = "dry-run"
	gitLabAPIPrivateTokenOption = "gitlab-api-private-token"
	gitLabBaseURLOption         = "gitlab-base-url"
)

var mainFlags = []cli.Flag{
	cli.StringFlag{
		Name:   gitLabAPIPrivateTokenOption,
		Usage:  "PAT for GitLab's API",
		EnvVar: "GITLAB_API_PRIVATE_TOKEN",
	},
	cli.StringFlag{
		Name:   gitLabBaseURLOption,
		Usage:  "Base URL of GitLab installation",
		EnvVar: "GITLAB_BASE_URL",
		Value:  gitLabBaseURL,
	},
	cli.BoolFlag{
		Name:  dryRunOption,
		Usage: "Don't perform real HTTP Requests against API",
	},
}

type Context interface{}

type defaultContext struct {
	*cli.Context
}

type App struct {
	app *cli.App
}

func (a *App) init() {
	cli.VersionPrinter = func(_ *cli.Context) {
		fmt.Println(rrhelper.VersionInstance().Extended())
	}

	app := cli.NewApp()
	app.Name = rrhelper.NAME
	app.Usage = "GitLab Runner release helper"
	app.Version = rrhelper.VersionInstance().ShortLine()
	app.Flags = append(app.Flags, mainFlags...)

	app.Before = func(cliCtx *cli.Context) error {
		return gitlab.ClientInitialize(
			cliCtx.GlobalString(gitLabBaseURLOption),
			cliCtx.GlobalString(gitLabAPIPrivateTokenOption),
			cliCtx.Bool(dryRunOption),
		)
	}

	a.app = app
}

func (a *App) AddCommands(commandDefinitions CommandDefinitions) {
	for _, commandDef := range commandDefinitions {
		a.AddCommand(commandDef)
	}
}

func (a *App) AddCommand(commandDef CommandDefinition) {
	a.app.Commands = append(a.app.Commands, NewCommand(commandDef))
}

func (a *App) Run(args ...string) {
	if len(args) < 1 {
		args = os.Args
	}

	if err := a.app.Run(args); err != nil {
		logrus.
			WithError(err).
			Fatal("Application execution failed")
	}
}

func New() *App {
	a := new(App)
	a.init()

	return a
}

func Recover() {
	r := recover()
	if r != nil {
		// log panics forces exit
		if _, ok := r.(*logrus.Entry); ok {
			os.Exit(1)
		}
		panic(r)
	}
}
