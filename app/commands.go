package app

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"

	"github.com/urfave/cli"
	clihelpers "gitlab.com/ayufan/golang-cli-helpers"

	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/gitlab"
)

type gitlabClientFactory func() (gitlab.Client, error)

type Actionable interface {
	Execute(appCtx Context) error
}

type GitLabClientUser interface {
	SetGitLabClient(client gitlab.Client)
}

type argumentsDefinition struct {
	Usage  string
	Before cli.BeforeFunc
}

type argumentDefinition struct {
	Index      int
	Name       string
	Field      reflect.StructField
	FieldValue reflect.Value
}

type CommandDefinition struct {
	Name   string
	Usage  string
	Action Actionable
}

type CommandDefinitions []CommandDefinition

func NewCommand(commandDef CommandDefinition) cli.Command {
	return NewCommandWithGitLabClientFactory(commandDef, gitlab.ClientInstance)
}

func NewCommandWithGitLabClientFactory(commandDef CommandDefinition, gitlabFactory gitlabClientFactory) cli.Command {
	command := newCliCommand(commandDef.Name, commandDef.Usage, commandDef.Action, gitlabFactory)

	args := getArgsFromStruct(commandDef.Action)
	if args == nil {
		return command
	}

	command.ArgsUsage = args.Usage
	command.Before = factorizeArgumentsCommandBefore(command.Before, args)

	return command
}

func newCliCommand(name string, usage string, commandObject Actionable, gitlabFactory gitlabClientFactory) cli.Command {
	return cli.Command{
		Name:  name,
		Usage: usage,
		Action: func(cliCtx *cli.Context) error {
			appCtx := &defaultContext{
				Context: cliCtx,
			}

			return commandObject.Execute(appCtx)
		},
		Flags:  clihelpers.GetFlagsFromStruct(commandObject),
		Before: factorizeMainCommandBefore(commandObject, gitlabFactory),
	}
}

func factorizeMainCommandBefore(commandObject Actionable, gitlabFactory gitlabClientFactory) cli.BeforeFunc {
	return func(cliCtx *cli.Context) error {
		object, ok := commandObject.(GitLabClientUser)
		if !ok {
			return nil
		}

		client, err := gitlabFactory()
		if err != nil {
			return fmt.Errorf("couldn't get GitLab Client instance: %v", err)
		}

		object.SetGitLabClient(client)

		return nil
	}
}

func factorizeArgumentsCommandBefore(internal cli.BeforeFunc, args *argumentsDefinition) cli.BeforeFunc {
	return func(cliCtx *cli.Context) error {
		if internal != nil {
			err := internal(cliCtx)
			if err != nil {
				return err
			}
		}

		if args.Before != nil {
			return args.Before(cliCtx)
		}

		return nil
	}
}

func getArgsFromStruct(data interface{}) *argumentsDefinition {
	return getArgsForValue(reflect.ValueOf(data))
}

var errValueUnsupported = errors.New("value unsupported")

func getArgsForValue(value reflect.Value) *argumentsDefinition {
	currentIndex := 0
	args := getArgsForValueWithIndex(value, &currentIndex)

	return newArgumentsDefinition(args)
}

func getArgsForValueWithIndex(value reflect.Value, currentIndex *int) []*argumentDefinition {
	value, err := prepareValue(value)
	if err == errValueUnsupported {
		return nil
	}

	valueType := value.Type()
	fieldsNumber := valueType.NumField()

	args := make([]*argumentDefinition, 0)

	for i := 0; i < fieldsNumber; i++ {
		newArgs := getArgsForValueField(currentIndex, valueType.Field(i), value.Field(i))
		if len(newArgs) == 0 {
			continue
		}

		args = append(args, newArgs...)
	}

	return args
}

func prepareValue(value reflect.Value) (reflect.Value, error) {
	if value.Type().Kind() == reflect.Ptr && value.Type().Elem().Kind() == reflect.Struct {
		if value.IsNil() {
			value.Set(reflect.New(value.Type().Elem()))
		}

		return reflect.Indirect(value), nil
	}

	if value.Type().Kind() != reflect.Struct {
		return value, errValueUnsupported
	}

	return value, nil
}

func getArgsForValueField(fieldIndex *int, field reflect.StructField, fieldValue reflect.Value) []*argumentDefinition {
	if !fieldValue.IsValid() {
		return nil
	}

	unsupportedTypes := map[reflect.Kind]bool{
		reflect.Ptr:           true,
		reflect.Chan:          true,
		reflect.Func:          true,
		reflect.Interface:     true,
		reflect.UnsafePointer: true,
	}

	if unsupportedTypes[field.Type.Kind()] {
		return nil
	}

	args := make([]*argumentDefinition, 0)
	if field.Type.Kind() == reflect.Struct {
		args = getArgsForValueWithIndex(fieldValue, fieldIndex)
	}

	tagName := field.Tag.Get("argument")
	if tagName != "" {
		args = append(args, &argumentDefinition{
			Index:      *fieldIndex,
			Name:       tagName,
			Field:      field,
			FieldValue: fieldValue,
		})

		*fieldIndex++
	}

	return args
}

func newArgumentsDefinition(args []*argumentDefinition) *argumentsDefinition {
	if len(args) < 1 {
		return nil
	}

	usage := make([]string, 0)
	for _, arg := range args {
		usage = append(usage, fmt.Sprintf("[%s]", arg.Name))
	}

	return &argumentsDefinition{
		Usage:  strings.Join(usage, " "),
		Before: factorizeArgumentsBefore(args),
	}
}

func factorizeArgumentsBefore(args []*argumentDefinition) cli.BeforeFunc {
	return func(cliCtx *cli.Context) error {
		if cliCtx.NArg() < len(args) {
			return fmt.Errorf("missing required arguments")
		}

		cliArgs := cliCtx.Args()

		for _, arg := range args {
			switch arg.Field.Type.Kind() {
			case reflect.String:
				arg.FieldValue.SetString(cliArgs.Get(arg.Index))
			case reflect.Int:
				argValue := cliArgs.Get(arg.Index)
				intArgValue, err := strconv.Atoi(argValue)
				if err != nil {
					panic(fmt.Sprintf("Couldn't transform %q into integer", argValue))
				}
				arg.FieldValue.SetInt(int64(intArgValue))
			}
		}

		return nil
	}
}
