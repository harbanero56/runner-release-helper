package commands

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/helpers/tag"
)

func Test_getReleaseTags(t *testing.T) {
	tests := []struct {
		runnerTagsList   string
		helmCharTagsList string
		expected         tag.ReleaseTags
		error            bool
	}{
		{
			runnerTagsList:   "13.2.2,13.1.2,13.0.1",
			helmCharTagsList: "0.19.2,0.18.2,0.17.1",
			expected: tag.ReleaseTags{
				{
					Runner: tag.GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        2,
					},
					HelmChart: tag.GitTag{
						StableBranch: "0-19-0-stable",
						Major:        0,
						Minor:        19,
						Patch:        2,
					},
				},
				{
					Runner: tag.GitTag{
						StableBranch: "13-1-stable",
						Major:        13,
						Minor:        1,
						Patch:        2,
					},
					HelmChart: tag.GitTag{
						StableBranch: "0-18-0-stable",
						Major:        0,
						Minor:        18,
						Patch:        2,
					},
				},
				{
					Runner: tag.GitTag{
						StableBranch: "13-0-stable",
						Major:        13,
						Minor:        0,
						Patch:        1,
					},
					HelmChart: tag.GitTag{
						StableBranch: "0-17-0-stable",
						Major:        0,
						Minor:        17,
						Patch:        1,
					},
				},
			},
		},
		{
			runnerTagsList:   "13.1.2,13.2.2,13.0.1",
			helmCharTagsList: "0.19.2,0.17.1,0.18.2",
			expected: tag.ReleaseTags{
				{
					Runner: tag.GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        2,
					},
					HelmChart: tag.GitTag{
						StableBranch: "0-19-0-stable",
						Major:        0,
						Minor:        19,
						Patch:        2,
					},
				},
				{
					Runner: tag.GitTag{
						StableBranch: "13-1-stable",
						Major:        13,
						Minor:        1,
						Patch:        2,
					},
					HelmChart: tag.GitTag{
						StableBranch: "0-18-0-stable",
						Major:        0,
						Minor:        18,
						Patch:        2,
					},
				},
				{
					Runner: tag.GitTag{
						StableBranch: "13-0-stable",
						Major:        13,
						Minor:        0,
						Patch:        1,
					},
					HelmChart: tag.GitTag{
						StableBranch: "0-17-0-stable",
						Major:        0,
						Minor:        17,
						Patch:        1,
					},
				},
			},
		},
		{
			runnerTagsList:   "13.2.2,13.1.2,13.0.1",
			helmCharTagsList: "",
			error:            true,
		},
		{
			runnerTagsList:   "13.2.2,13.1.2,13.0.1",
			helmCharTagsList: "0.18.2",
			error:            true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.runnerTagsList+"/"+tt.helmCharTagsList, func(t *testing.T) {
			got, err := getReleaseTags(tt.runnerTagsList, tt.helmCharTagsList)
			if tt.error {
				assert.Error(t, err)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, tt.expected, got)
		})
	}
}
