package commands

import (
	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/app"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/gitlab"
)

func NewCreateBranch() *CreateBranch {
	return &CreateBranch{
		base: new(base),
	}
}

type CreateBranch struct {
	*base

	ProjectID string `argument:"project_id"`
	Name      string `argument:"branch_name"`
}

func (c *CreateBranch) Execute(appCtx app.Context) error {
	err := c.checkGitLabClient()
	if err != nil {
		return err
	}

	params := gitlab.CreateBranchParams{
		ProjectID: c.ProjectID,
		Name:      c.Name,
		Ref:       "master",
	}
	branch, err := c.client.CreateBranch(params)
	if err != nil {
		return err
	}

	logrus.Printf("Successfully created branch %q in %q project", branch.Name, branch.Project)

	return nil
}
