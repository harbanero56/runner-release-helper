package commands

import (
	"errors"
	"fmt"
	"sort"
	"strings"

	"github.com/hashicorp/go-version"
	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/app"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/gitlab"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/helpers/tag"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/helpers/template"
)

func NewCreateSecurityReleaseChecklist() *CreateSecurityReleaseChecklist {
	return &CreateSecurityReleaseChecklist{
		base: new(base),
	}
}

type CreateSecurityReleaseChecklist struct {
	*base
	Tags                       tag.ReleaseTags
	SecurityReleaseChecklistID int

	RunnerTags    string `long:"runner-tags" description:"List of comma-separated tags for the Runner"`
	HelmChartTags string `long:"helm-tags" description:"List of comma-separated tags corresponding with Runner tags"`
	ProjectID     string `long:"project-id" description:"The ID of the project where the issue will be created"`
	SecurityURL   string `long:"security-url" description:"The URL for the GitLab Security Issue"`
}

func (s *CreateSecurityReleaseChecklist) Execute(_ app.Context) error {
	err := s.checkGitLabClient()
	if err != nil {
		return err
	}

	s.Tags, err = getReleaseTags(s.RunnerTags, s.HelmChartTags)
	if err != nil {
		return fmt.Errorf("generating release tags: %w", err)
	}

	// Create initial release so that we can get the Issue ID
	initialIssueDescription, err := s.getInitialIssueDescription()
	if err != nil {
		return fmt.Errorf("parsing initial issue description: %w", err)
	}

	initialIssueParams := gitlab.CreateIssueParams{
		ProjectID:   s.ProjectID,
		Title:       fmt.Sprintf("Security Release: %s", s.RunnerTags),
		Description: initialIssueDescription,
	}

	issue, err := s.client.CreateIssue(initialIssueParams)
	if err != nil {
		return fmt.Errorf("creating issue: %w", err)
	}

	s.SecurityReleaseChecklistID = issue.IID

	desc, err := s.getIssueDescription()
	if err != nil {
		return fmt.Errorf("parsing initial issue description: %w", err)
	}

	updateIssue := gitlab.UpdateIssueParams{
		ProjectID:   s.ProjectID,
		IID:         s.SecurityReleaseChecklistID,
		Title:       issue.Title,
		Description: desc,
	}

	_, err = s.client.UpdateIssue(updateIssue)
	if err != nil {
		return fmt.Errorf("update issue: %w", err)
	}

	logrus.Printf("Successfully created issue %q at %s", issue.Title, issue.WebURL)

	return nil
}

func (s *CreateSecurityReleaseChecklist) getInitialIssueDescription() (string, error) {
	tpl, err := template.Load(template.TypeIssue, "security/initial")
	if err != nil {
		return "", err
	}

	return tpl.Execute(s)
}

func (s *CreateSecurityReleaseChecklist) getIssueDescription() (string, error) {
	tpl, err := template.Load(template.TypeIssue, "security/release")
	if err != nil {
		return "", err
	}

	partials := []string{
		"merge",
		"tags",
		"gitlab-managed-apps",
		"deploy",
		"announcement",
		"sync",
		"upstream-dependencies",
	}

	for _, name := range partials {
		_, err = tpl.LoadPart(name)
		if err != nil {
			return "", fmt.Errorf("loading template partials %q: %v", name, err)
		}
	}

	return tpl.Execute(s)
}

func getReleaseTags(runnerTagsList, helmChartTagsList string) (tag.ReleaseTags, error) {
	runnerTags, err := getGitTags(runnerTagsList)
	if err != nil {
		return nil, fmt.Errorf("parsing runner-tags: %w", err)
	}

	helmChartTags, err := getGitTags(helmChartTagsList)
	if err != nil {
		return nil, fmt.Errorf("parsing helm-tags: %w", err)
	}

	if len(runnerTags) != len(helmChartTags) {
		return nil, errors.New("runner and helm chart tags are not the same length")
	}

	var tags tag.ReleaseTags
	for i, runnerTag := range runnerTags {
		runnerTag.StableBranch = getRunnerStableBranch(runnerTag)
		rt := tag.ReleaseTag{
			Runner: runnerTag,
		}

		rt.HelmChart = helmChartTags[i]
		rt.HelmChart.StableBranch = getHelmChartStableBranch(rt.HelmChart)

		tags = append(tags, rt)
	}

	return tags, nil
}

func getGitTags(tagList string) (tag.GitTags, error) {
	splitTags := strings.Split(tagList, ",")

	var tags tag.GitTags
	for _, t := range splitTags {
		if t == "" {
			continue
		}

		ver, err := version.NewVersion(strings.TrimSpace(t))
		if err != nil {
			return nil, err
		}

		segments := ver.Segments()

		tags = append(tags, tag.GitTag{
			Major: segments[0],
			Minor: segments[1],
			Patch: segments[2],
		})
	}

	sort.Sort(sort.Reverse(tags))

	return tags, nil
}

func getHelmChartStableBranch(tag tag.GitTag) string {
	return fmt.Sprintf("%d-%d-0-stable", tag.Major, tag.Minor)
}

func getRunnerStableBranch(tag tag.GitTag) string {
	return fmt.Sprintf("%d-%d-stable", tag.Major, tag.Minor)
}
