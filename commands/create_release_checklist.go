package commands

import (
	"fmt"
	"strings"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/app"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/gitlab"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/helpers/console"
	"gitlab.com/gitlab-org/ci-cd/runner-release-helper/helpers/template"
)

func NewCreateReleaseChecklist() *CreateReleaseChecklist {
	return &CreateReleaseChecklist{
		checklistCommon: newChecklistCommon(),
		RC:              1,
	}
}

type CreateReleaseChecklist struct {
	checklistCommon

	ReleaseManagerHandle string `long:"release-manager-handle" description:"GitLab.com handle of the Release Manager" env:"GITLAB_RUNNER_RELEASE_MANAGER_HANDLE"`

	ReplaceLinkInPreviousReleaseChecklistIssue string `long:"replace-link-in-previous-release-checklist-issue" description:"If the value is present, it will be replaced in the Previous Release Checklist issue by the WebURL of created newly created Release Checklist issue"`

	ReleaseChecklistIssueID         int
	PreviousReleaseCheckListIssueID int `long:"previous-release-checklist-issue" description:"ID of the Release Checklist issue for previous release"`

	// Variables required for feeding the RC1 partial
	// These are configurable in the `create-next-rc-checklist` command
	RC int
}

func (c *CreateReleaseChecklist) Execute(appCtx app.Context) error {
	err := c.checkGitLabClient()
	if err != nil {
		return err
	}

	if c.NonInteractive {
		logrus.Print("Running in non-interactive mode")
	}

	err = c.autodetect()
	if err != nil {
		return err
	}

	questions := []valueQuestion{
		{prompt: "GitLab.com handle of the release manager", value: &c.ReleaseManagerHandle, requester: console.NewStringRequester},
	}

	err = c.askForValues(questions)
	if err != nil {
		return fmt.Errorf("error while performing value questions: %v", err)
	}

	return c.create()
}

func (c *CreateReleaseChecklist) create() error {
	printIssueFn := func() error {
		c.ReleaseChecklistIssueID = -100
		description, err := c.prepareDescription()
		if err != nil {
			return err
		}

		fmt.Println(description)

		return nil
	}

	return c.guardCreate(c.createIssue, printIssueFn)
}

func (c *CreateReleaseChecklist) createIssue() error {
	title := fmt.Sprintf("GitLab Runner %d.%d release checklist", c.Major, c.Minor)

	initialDescription, err := c.prepareInitialDescription()
	if err != nil {
		return err
	}

	createIssueParams := gitlab.CreateIssueParams{
		ProjectID:   c.RunnerProjectID,
		Title:       title,
		Description: initialDescription,
	}
	issue, err := c.client.CreateIssue(createIssueParams)
	if err != nil {
		return err
	}

	c.ReleaseChecklistIssueID = issue.IID
	description, err := c.prepareDescription()
	if err != nil {
		return err
	}

	updateIssueParams := gitlab.UpdateIssueParams{
		ProjectID:   c.RunnerProjectID,
		IID:         issue.IID,
		Title:       issue.Title,
		Description: description,
	}
	issue, err = c.client.UpdateIssue(updateIssueParams)
	if err != nil {
		return err
	}

	logrus.Printf("Successfully created issue %q at %s", issue.Title, issue.WebURL)

	return c.updateReleaseChecklistIssueWithNewIssueWebURL(issue)
}

func (c *CreateReleaseChecklist) prepareInitialDescription() (string, error) {
	tpl, err := template.Load(template.TypeIssue, "release-checklist-initial")
	if err != nil {
		return "", err
	}

	return tpl.Execute(c)
}

func (c *CreateReleaseChecklist) prepareDescription() (string, error) {
	tpl, err := template.Load(template.TypeIssue, "release-checklist")
	if err != nil {
		return "", err
	}

	partials := []string{
		"release-day",
		"release-finalization",
	}

	for _, name := range partials {
		_, err = tpl.LoadPart(name)
		if err != nil {
			return "", fmt.Errorf("error while loading template part %q: %v", name, err)
		}
	}

	rcChecklistTpl, err := tpl.LoadPart("rc-checklist")
	if err != nil {
		return "", fmt.Errorf("error while loading template part %q: %v", "rc-checklist", err)
	}

	_, err = rcChecklistTpl.LoadPart("rc-mrs-list")
	if err != nil {
		return "", fmt.Errorf("error while loading template part %q: %v", "rc-mrs-list", err)
	}

	return tpl.Execute(c)
}

func (c *CreateReleaseChecklist) updateReleaseChecklistIssueWithNewIssueWebURL(issue gitlab.Issue) error {
	if c.PreviousReleaseCheckListIssueID == 0 || c.ReplaceLinkInPreviousReleaseChecklistIssue == "" {
		logrus.Warning("ReleaseChecklistIssueID or ReplaceLinkInPreviousReleaseChecklistIssue undefined; skipping issue update")
		return nil
	}

	readIssueParams := gitlab.ReadIssueParams{
		ProjectID: c.RunnerProjectID,
		IID:       c.PreviousReleaseCheckListIssueID,
	}
	previousIssue, err := c.client.ReadIssue(readIssueParams)
	if err != nil {
		return err
	}

	updateIssueParams := gitlab.UpdateIssueParams{
		ProjectID:   c.RunnerProjectID,
		IID:         previousIssue.IID,
		Title:       previousIssue.Title,
		Description: strings.Replace(previousIssue.Description, c.ReplaceLinkInPreviousReleaseChecklistIssue, issue.WebURL, 1),
	}
	previousIssue, err = c.client.UpdateIssue(updateIssueParams)
	if err != nil {
		return err
	}

	logrus.Printf("Successfully updated issue %q at %s", previousIssue.Title, previousIssue.WebURL)

	return nil
}
