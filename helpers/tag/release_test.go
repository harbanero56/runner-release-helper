package tag

import (
	"fmt"
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReleaseTag_Sort(t *testing.T) {
	tests := []struct {
		tags         ReleaseTags
		expectedTags ReleaseTags
	}{
		{
			tags: ReleaseTags{
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        1,
					},
					HelmChart: GitTag{
						StableBranch: "0-19-0-stable",
						Major:        0,
						Minor:        19,
						Patch:        1,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-0-stable",
						Major:        13,
						Minor:        0,
						Patch:        1,
					},
					HelmChart: GitTag{
						StableBranch: "0-17-0-stable",
						Major:        0,
						Minor:        17,
						Patch:        1,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-1-stable",
						Major:        13,
						Minor:        1,
						Patch:        2,
					},
					HelmChart: GitTag{
						StableBranch: "0-18-0-stable",
						Major:        0,
						Minor:        18,
						Patch:        2,
					},
				},
			},
			expectedTags: ReleaseTags{
				{
					Runner: GitTag{
						StableBranch: "13-0-stable",
						Major:        13,
						Minor:        0,
						Patch:        1,
					},
					HelmChart: GitTag{
						StableBranch: "0-17-0-stable",
						Major:        0,
						Minor:        17,
						Patch:        1,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-1-stable",
						Major:        13,
						Minor:        1,
						Patch:        2,
					},
					HelmChart: GitTag{
						StableBranch: "0-18-0-stable",
						Major:        0,
						Minor:        18,
						Patch:        2,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        1,
					},
					HelmChart: GitTag{
						StableBranch: "0-19-0-stable",
						Major:        0,
						Minor:        19,
						Patch:        1,
					},
				},
			},
		},
		{
			tags: ReleaseTags{
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        0,
					},
					HelmChart: GitTag{
						StableBranch: "0-19-0-stable",
						Major:        0,
						Minor:        19,
						Patch:        0,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        2,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        1,
					},
				},
			},
			expectedTags: ReleaseTags{
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        0,
					},
					HelmChart: GitTag{
						StableBranch: "0-19-0-stable",
						Major:        0,
						Minor:        19,
						Patch:        0,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        1,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        2,
					},
				},
			},
		},
		{
			tags: ReleaseTags{
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
					},
				},
			},
			expectedTags: ReleaseTags{
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        0,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        0,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        0,
					},
				},
			},
		},
	}
	for i, tt := range tests {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			sort.Sort(tt.tags)
			assert.Equal(t, tt.expectedTags, tt.tags)
		})
	}
}

func TestReleaseTags_Latest(t *testing.T) {
	tests := []struct {
		tags        ReleaseTags
		expectedTag ReleaseTag
	}{
		{
			tags: ReleaseTags{
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        1,
					},
					HelmChart: GitTag{
						StableBranch: "0-19-0-stable",
						Major:        0,
						Minor:        19,
						Patch:        1,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-0-stable",
						Major:        13,
						Minor:        0,
						Patch:        1,
					},
					HelmChart: GitTag{
						StableBranch: "0-17-0-stable",
						Major:        0,
						Minor:        17,
						Patch:        1,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-1-stable",
						Major:        13,
						Minor:        1,
						Patch:        2,
					},
					HelmChart: GitTag{
						StableBranch: "0-18-0-stable",
						Major:        0,
						Minor:        18,
						Patch:        2,
					},
				},
			},
			expectedTag: ReleaseTag{
				Runner: GitTag{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        1,
				},
				HelmChart: GitTag{
					StableBranch: "0-19-0-stable",
					Major:        0,
					Minor:        19,
					Patch:        1,
				},
			},
		},
		{
			tags: ReleaseTags{
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        0,
					},
					HelmChart: GitTag{
						StableBranch: "0-19-0-stable",
						Major:        0,
						Minor:        19,
						Patch:        0,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        2,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
						Patch:        1,
					},
				},
			},
			expectedTag: ReleaseTag{
				Runner: GitTag{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        2,
				},
			},
		},
		{
			tags: ReleaseTags{
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
					},
				},
				{
					Runner: GitTag{
						StableBranch: "13-2-stable",
						Major:        13,
						Minor:        2,
					},
				},
			},
			expectedTag: ReleaseTag{
				Runner: GitTag{
					StableBranch: "13-2-stable",
					Major:        13,
					Minor:        2,
					Patch:        0,
				},
			},
		},
	}
	for i, tt := range tests {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			latest := tt.tags.Latest()
			assert.Equal(t, tt.expectedTag, latest)
		})
	}
}
