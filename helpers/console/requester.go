package console

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type InputRequesterFactory func(nonInteractive bool, value interface{}) (InputRequester, error)

type InputRequester interface {
	Ask(prompt string) error
}

type defaultRequester struct {
	nonInteractive bool
	reader         *bufio.Reader
}

func (r *defaultRequester) ask(prompt string, value *string) error {
	fmt.Printf("%s [%s]: ", prompt, *value)
	if r.nonInteractive {
		fmt.Println()

		return isValueEmpty(value)
	}

	data, _, err := r.getReader().ReadLine()
	if err != nil {
		return err
	}

	newValue := string(data)
	newValue = strings.TrimSpace(newValue)

	if newValue != "" {
		*value = newValue
	}

	return isValueEmpty(value)
}

func isValueEmpty(value *string) error {
	if *value == "" {
		return fmt.Errorf("value can't be left empty")
	}

	return nil
}

func (r *defaultRequester) getReader() *bufio.Reader {
	if r.reader == nil {
		r.reader = bufio.NewReader(os.Stdin)
	}

	return r.reader
}

type IntRequester struct {
	*defaultRequester
	value *int
}

func (r *IntRequester) Ask(prompt string) error {
	valueString := strconv.Itoa(*r.value)

	err := r.ask(prompt, &valueString)
	if err != nil {
		return err
	}

	*r.value, err = strconv.Atoi(valueString)

	return err
}
func NewIntRequester(nonInteractive bool, value interface{}) (InputRequester, error) {
	intValue, ok := value.(*int)
	if !ok {
		return nil, fmt.Errorf("can't type cast the value %v to int", value)
	}

	requester := &IntRequester{
		defaultRequester: &defaultRequester{
			nonInteractive: nonInteractive,
		},
		value: intValue,
	}

	return requester, nil
}

type StringRequester struct {
	*defaultRequester
	value *string
}

func (r *StringRequester) Ask(prompt string) error {
	return r.ask(prompt, r.value)
}

func NewStringRequester(nonInteractive bool, value interface{}) (InputRequester, error) {
	stringValue, ok := value.(*string)
	if !ok {
		return nil, fmt.Errorf("can't type cast the value %v to string", value)
	}

	requester := &StringRequester{
		defaultRequester: &defaultRequester{
			nonInteractive: nonInteractive,
		},
		value: stringValue,
	}

	return requester, nil
}
