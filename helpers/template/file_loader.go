package template

import (
	"fmt"
	"io/ioutil"
)

type fileLoader struct {
	basePath string
}

func (l *fileLoader) Load(templateType Type, path string) ([]byte, error) {
	templatePath := fmt.Sprintf("%s/%s/%s", l.basePath, templateType, path)

	data, err := ioutil.ReadFile(templatePath)
	if err != nil {
		return nil, fmt.Errorf("error while reading the template from file %q: %v", templatePath, err)
	}

	return data, nil
}

func newFileLoader() loader {
	return &fileLoader{
		basePath: "./templates",
	}
}
