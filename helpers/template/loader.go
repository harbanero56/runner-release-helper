package template

import (
	"fmt"
)

const (
	loaderTypeFile    string = "file"
	loaderTypeBindata string = "bindata"
)

var usedLoaderType = loaderTypeFile

type Type string

const (
	TypeIssue        Type = "issues"
	TypeMergeRequest Type = "merge_requests"
)

type LoaderFactory func() loader

type loader interface {
	Load(templateType Type, path string) ([]byte, error)
}

var usedLoader loader

func getLoader() (loader, error) {
	if usedLoader != nil {
		return usedLoader, nil
	}

	loader, err := newLoader()
	if err != nil {
		return nil, err
	}

	usedLoader = loader

	return usedLoader, nil
}

func newLoader() (loader, error) {
	loaders := map[string]LoaderFactory{
		loaderTypeFile:    newFileLoader,
		loaderTypeBindata: newBindataLoader,
	}

	loaderFactory, ok := loaders[usedLoaderType]
	if !ok {
		return nil, fmt.Errorf("unknown template loader type %q", usedLoaderType)
	}

	return loaderFactory(), nil
}
